import time
import pytest
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.remote.webdriver import WebDriver


base_url = "https://acm.timus.ru/?locale=en"


def test_find_problem(selenium: WebDriver):
    selenium.get(base_url)
    assert selenium.title == "Timus Online Judge"

    problem_set = selenium.find_element(
        By.XPATH,
        "//a[contains(text(), 'Problem set')]"
    )

    problem_set.click()

    volume_4 = selenium.find_element(
        By.XPATH,
        "//a[contains(text(), 'Volume 4')]"
    )

    volume_4.click()

    convex_hull = selenium.find_element(
        By.XPATH,
        "//a[contains(text(), 'Convex Hull')]"
    )

    convex_hull.click()

    time.sleep(1)

    assert "Convex Hull" in selenium.title


def test_find_author(selenium: WebDriver):
    selenium.get(base_url)
    assert selenium.title == "Timus Online Judge"

    search_form = selenium.find_element(
        By.XPATH,
        "//input[@name='Str']"
    )

    search_form.send_keys("Igor Parfenov")
    search_form.send_keys(Keys.ENTER)

    time.sleep(1)

    author = selenium.find_element(
        By.XPATH,
        "//a[contains(text(), 'Igor Parfenov')]"
    )

    author.click()
    
    assert "Igor Parfenov" in selenium.title


def test_check_scheduled_contests(selenium: WebDriver):
    selenium.get(base_url)
    assert selenium.title == "Timus Online Judge"

    scheduled_contests = selenium.find_element(
        By.XPATH,
        "//a[contains(text(), 'Scheduled contests')]"
    )

    scheduled_contests.click()

    # Never lose hope
    selenium.find_element(
        By.XPATH,
        "//*[contains(text(), 'No contests scheduled at the moment')]"
    )
