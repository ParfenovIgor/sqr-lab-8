# Lab 8 - Exploratory testing and UI

[![pipeline status](https://gitlab.com/ParfenovIgor/sqr-lab-8/badges/master/pipeline.svg)](https://gitlab.com/ParfenovIgor/sqr-lab-8/-/commits/master)

## Author

Igor Parfenov

Contact: [@Igor_Parfenov](https://t.me/Igor_Parfenov)

## Exploratory testing and UI

I chosen [Timue Online Judge](https://acm.timus.ru/?locale=en).
The test report follows.

## 1. Find Problem

| Action | Status/Comment |
| - | - |
| Go to `acm.timus.ru` and check if title is *Timus Online Judge* | ✅ |
| Find button *Problem set* in navigation and click on it | ✅ |
| Find button *Volume 4* in body and click on it | ✅ |
| Find button *Convex Hull* in table and click on it | ✅ |
| Check if title contains *Convex Hull* | ✅ |

## 2. Find Author

| Action | Status/Comment |
| - | - |
| Go to `acm.timus.ru` and check if title is *Timus Online Judge* | ✅ |
| Find a text field with name `Str` which is responsible for authors search | ✅ |
| Enter *Igor Parfenov* into text field and press *Enter* | ❔ You need to wait here |
| Find button *Igor parfenov* in table and click on it | ✅ |
| Check if title contains *Igor Parfenov* | ✅ |

## 3. Check Scheduled Contests

| Action | Status/Comment |
| - | - |
| Go to `acm.timus.ru` and check if title is *Timus Online Judge* | ✅ |
| Find button *Scheduled contests* in navigation and click on it | ✅ |
| Check if there is text *No contests scheduled at the moment* | ❗ Information is upsetting |